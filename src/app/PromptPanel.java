package app;

import java.awt.GridLayout;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

public class PromptPanel extends JPanel {

	private static final long serialVersionUID = 1L;
	private JTextField addrField;
	private JTextField portField;
	private JRadioButton servidorButton;
	private JRadioButton clienteButton;

	public PromptPanel() {
		initVariables();
		initLayout();
		initListeners();
	}

	private void initListeners() {
		clienteButton.addActionListener(e -> {
			servidorButton.setSelected(!clienteButton.isSelected());
			enableAdress(clienteButton.isSelected());
			addrField.setEnabled(clienteButton.isSelected());
		});
		servidorButton.addActionListener(e -> {
			clienteButton.setSelected(!servidorButton.isSelected());
			enableAdress(clienteButton.isSelected());
		});
	}
	
	private void enableAdress(boolean enable) {
		addrField.setEnabled(enable);
		if (!enable)
			addrField.setText("127.0.0.1");
	}

	private void initVariables() {
		addrField = new JTextField(10);
		addrField.setText("127.0.0.1");
		portField = new JTextField(5);
		servidorButton = new JRadioButton("Servidor");
		clienteButton = new JRadioButton("Cliente", true);

	}

	private void initLayout() {
		setLayout(new GridLayout(3, 2));
		add(new JLabel("Endereço:"));
		add(addrField);
		add(new JLabel("Porta:"));
		add(portField);
		add(servidorButton);
		add(clienteButton);
	}

	public String getAddr() {
		return addrField.getText();
	}

	public String getPort() {
		return portField.getText();
	}
	
	public boolean isClient() {
		return clienteButton.isSelected();
	}

}
