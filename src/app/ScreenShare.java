package app;

import java.awt.AWTException;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.MouseInfo;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;

import javax.imageio.ImageIO;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class ScreenShare {

	private static String serverAddr;
	private static String port;
	private ServerSocket server;
	private Socket socket;

	public static void main(String[] args) {
		new ScreenShare().prompt();
	}

	private void prompt() {
		PromptPanel prompt = new PromptPanel();
		int res = JOptionPane.showConfirmDialog(null, prompt, "ShareScreen", JOptionPane.OK_CANCEL_OPTION,
				JOptionPane.PLAIN_MESSAGE);
		serverAddr = prompt.getAddr();
		port = prompt.getPort();
		System.out.println(prompt.getAddr());
		System.out.println(prompt.getPort());
		if (res == JOptionPane.CANCEL_OPTION)
			return;
		else if(port.isEmpty() || serverAddr.isEmpty())
			return;
		else if(prompt.isClient())
			client(serverAddr, Integer.parseInt(port));
		else
			server(Integer.parseInt(port));
	}

	private void client(String serverAddr, int port) {
		JFrame frame = new JFrame();
		ImagePanel panel = new ImagePanel();
		frame.setResizable(true);
		frame.add(panel);
		frame.pack();
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		try {
			BufferedImage image;
			while (true) {
				socket = new Socket(serverAddr, port);
				ObjectInputStream inputStream = new ObjectInputStream(socket.getInputStream());
				image = ImageIO.read(inputStream);
				panel.setImg(image);
				panel.repaint();
				socket.close();
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void server(int port) {
		try {
			server = new ServerSocket(port);
			Robot r = new Robot();

			while (true) {
				try {
					socket = server.accept();
					InetAddress addr = socket.getInetAddress();
					System.out.println(
							"Received Connection From " + addr.getCanonicalHostName() + " at " + addr.getHostAddress());
					ObjectOutputStream outstream = new ObjectOutputStream(socket.getOutputStream());
					BufferedImage img;
					img = r.createScreenCapture(new Rectangle(Toolkit.getDefaultToolkit().getScreenSize()));
					Point mouse = MouseInfo.getPointerInfo().getLocation();
					Graphics g = img.getGraphics();
					g.setColor(Color.BLACK);
					g.fillRect(mouse.x, mouse.y, 30, 30);
					ImageIO.write(img, "jpg", outstream);
					socket.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		} catch (AWTException e) {
			e.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}

	public static BufferedImage resize(BufferedImage img, int newW, int newH) {
		Image tmp = img.getScaledInstance(newW, newH, Image.SCALE_SMOOTH);
		BufferedImage dimg = new BufferedImage(newW, newH, BufferedImage.TYPE_INT_ARGB);
		Graphics2D g2d = dimg.createGraphics();
		g2d.drawImage(tmp, 0, 0, null);
		g2d.dispose();
		return dimg;
	}
}
